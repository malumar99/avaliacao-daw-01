const express = require('express');
const app = express();
const port = 3000;
const path = require('path');


app.use(express.static("public"));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/html/index.html'));
})


app.get('/index', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/html/index.html'));
})

app.get('/duvidas', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/html/duvida.html'));
})

app.get('/eu', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/html/eu.html'));
})


app.get('/alunos', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/html/aluno.html'));
})

app.get('/lista-alunos', function (req, res) {
    var fs = require('fs');
    var path = require('path');

    var data = fs.readFileSync(path.join(__dirname, '/public/json/') +'alunos.json', 'utf8');
    //console.log(data.toString()); 
    res.json(data);

})


app.get('/confirmacao', function (req, res) {
    res.send("Obrigado por ter enviado a mensagem '"+ req.query.mensagem + "'. Retornaremos no e-mail '"+ req.query.email +"'.");
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
})