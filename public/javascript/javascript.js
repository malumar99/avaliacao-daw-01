function data() {
    console.log('data');
}


function buscaAlunos() {
    console.log('aa');


    let arrayResultado = [];

    let inputPesquisa = document.getElementById('parametro').value;

    $.ajax({
        url: "/lista-alunos",
        type: 'GET'
    })
        .done(function (json) {
            json = JSON.parse(json);
            //debugger;
            let html = "";
            for (let i in json) {
                console.log(json[i].nome)
                if(inputPesquisa == '' || (json[i].nome.substring(0, inputPesquisa.length).toUpperCase() == inputPesquisa.toUpperCase())){
                    arrayResultado.push(json[i]);
                }
            }


            html += `<table class="table" border="1">
                <tr>
                    <th>Nome</th>
                    <th>E-mail</th>
                </tr>`;

                let contador = 1;
                let cor = '#f2f2f2'
                
                if(arrayResultado.length >= 1){
                    for (let n in arrayResultado) {
                        if(contador%2 == 0){
                            cor = '#f2f2f2'
                        }else{
                            cor = '#ffffff'
                        }
                        contador++;
                        
                        html += `   <tr style="background-color:${cor}">
                                        <td>${arrayResultado[n].nome}</td>
                                        <td>${arrayResultado[n].email}</td>
                                    </tr>`;

                    }
                }else{
                    html += `   <tr style="background-color:${cor}">
                                        <td colspan="7">Nenhum resultado encontrado</td>
                                    </tr>`;

                }

            html += `</table>`;
            $("#recebeTabela").html(html);
        })
        .fail(function (jqXHR, textStatus, msg) {
            alert(msg);
        });

}